import requests
import json
from tests_settings import NewUser
from tests_settings import Test_data
from requests.packages.urllib3.exceptions import InsecureRequestWarning


user_session = requests.Session() 
user_session.verify = False
step_results = {}
###########PRECONDITION#############
#Creation of the new user (with data from tests_settings)
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
user_session.post(url = 'https://localhost:5000/login', json = {'login': 'admin', 'password': 'admin'})

user_session.post(url = 'https://localhost:5000/admin/recreatedb')

user_session.post(url = 'https://localhost:5000/admin/user', json = {'new_login': NewUser.login, 'new_password': NewUser.password})
user_session.post(url = 'https://localhost:5000/logout')
#Login as new just created user
if user_session.post(url = 'https://localhost:5000/login', json = {'login': NewUser.login, 'password': NewUser.password}):
    step_results['Precondition: login as new created user'] = 'passed'
else:
    step_results['Precondition: login as new created user'] = 'failed'

##############STEP1################
#adding the requirements data
for i in Test_data.requirements_data:
    user_session.post(url = 'https://localhost:5000/req', json = i)
if user_session.get(url = 'https://localhost:5000/reqs'):
    step_results['Adding requrements are '] = 'passed'
else:
    step_results['Adding requrements are '] = 'failed'

#adding the TCs
for i in Test_data.test_cases_data:
    user_session.post(url = 'https://localhost:5000/testcase', json = i)
if user_session.get(url = 'https://localhost:5000/testcases'):
    step_results['Adding testcases are '] = 'passed'
else:
    step_results['Adding testcases are '] = 'failed'

#adding of the run_data
testrun_id = user_session.post(url = 'https://localhost:5000/testrun', json = Test_data.test_run_data)
run_id = json.loads(testrun_id.text)
if user_session.get(url = 'https://localhost:5000/testruns'):
    step_results['Adding Test run is '] = 'passed'
else:
    step_results['Adding Test run is '] = 'failed'

#adding of the results
for i in Test_data.test_result_data:
    user_session.post(url = 'https://localhost:5000/testrun/'+str(run_id['id'])+'/result', json = i)

###############STEP2##################
#Creating the report
response = user_session.get(url = 'https://localhost:5000/testrun/'+str(run_id['id'])+'/pdf')
with open('report2.pdf', 'wb') as f:
    f.write(response.content)
if response:
    step_results['Report generation is '] = 'passed'
else:
    step_results['Report generation is '] = 'failed'

###############STEP3##################



user_session.post(url = 'https://localhost:5000/logout')


if 'failed' in step_results.values():
    print('Test FAILED')
    for key, value in step_results.items():
        if value == 'failed':
            print(key, value)
    
else:
    print('Test PASSED')