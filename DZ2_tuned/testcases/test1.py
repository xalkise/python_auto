import requests
from tests_settings import User
from requests.packages.urllib3.exceptions import InsecureRequestWarning

user_session = requests.Session() 
user_session.verify = False

step_results = {}
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

#Login as admin
if user_session.post(url = 'https://localhost:5000/login', json = {'login': 'admin', 'password': 'admin'}):
    step_results['login as admin'] = 'passed'
else:
    step_results['login as admin'] = 'failed'

#Creation of the new user
if user_session.post(url = 'https://localhost:5000/admin/user', json = {'new_login': User.new_user, 'new_password': User.new_password}):
    step_results['new user creation in db'] = 'passed'
else:
    step_results['new user creation in db'] = 'failed'

#Logout admin
if user_session.post(url = 'https://localhost:5000/logout'):
    step_results['admin logout'] = 'passed'
else:
    step_results['admin logout'] = 'failed'

#Login as just created user
if user_session.post(url = 'https://localhost:5000/login', json = {'login': User.new_user, 'password': User.new_password}):
    step_results['login as new created user'] = 'passed'
else:
    step_results['login as new created user'] = 'failed'


#TC result
if 'failed' in step_results.values():
    print('Test FAILED')
    for key, value in step_results.items():
        if value == 'failed':
            print(key, value)
    
else:
    print('Test PASSED')
