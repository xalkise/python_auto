import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requirements_data = [
    {"name": "TSR_01", "risk": 5},
    {"name": "TSR_02", "risk": 3},
    {"name": "TSR_03", "risk": 2},
    {"name": "TSR_04", "risk": 1},
    {"name": "TSR_05", "risk": 1},
    {"name": "TSR_06", "risk": 2},
]
test_cases_data = [
    # Одно требование
    {"name": "TC_0001", "requirements": ["TSR_01"]},
    # Покрытие требований не заведено в системе
    {"name": "TC_0002", "requirements": []},
    # Несколько требований, разные уровни риска
    {"name": "TC_0003", "requirements": ["TSR_02", "TSR_03", "TSR_04"]},
    # Несколько требований, один уровень риска
    {"name": "TC_0004", "requirements": ["TSR_04", "TSR_05"]},
    # Тест, который не будет включен в набор
    {"name": "TC_0005", "requirements": ["TSR_05", "TSR_06"]},
]
test_run_data = {
    "start_date": "2018-03-12",
    "end_date": "2018-03-19",
    "description": "Regression Testing",
    "test_cases": ["TC_0001", "TC_0002", "TC_0003", "TC_0004"]
}

test_result_data = [
    {"name": "TC_0001", "is_passed": True, "date_time": "2018-03-13T00:00:00Z"},
    {"name": "TC_0002", "is_passed": True, "date_time": "2018-03-13T23:59:59Z"},
    {"name": "TC_0003", "is_passed": False, "date_time": "2018-03-15T10:43:21Z"},
    # TC_0004 не выполнен
    # Невалидная запись
    {"name": "TC_0005", "is_passed": True, "date_time": "2018-03-20T00:00:00Z"},
]
user_session = requests.Session() 
user_session.verify = False
##############STEP1################
#Creation of the new user "some_user"
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
user_session.post(url = 'https://localhost:5000/login', json = {'login': 'admin', 'password': 'admin'})

user_session.post(url = 'https://localhost:5000/admin/recreatedb')

user_session.post(url = 'https://localhost:5000/admin/user', json = {'new_login': 'some_user', 'new_password': 'some_password'})
user_session.post(url = 'https://localhost:5000/logout')
user_session.post(url = 'https://localhost:5000/login', json = {'login': 'some_user', 'password': 'some_password'})

#adding the test data
for i in requirements_data:
    user_session.post(url = 'https://localhost:5000/req', json = i)

#adding of the TCs
for i in test_cases_data:
    user_session.post(url = 'https://localhost:5000/testcase', json = i)

#adding of the run_data
testrun_id = user_session.post(url = 'https://localhost:5000/testrun', json = test_run_data)
run_id = json.loads(testrun_id.text)
print(run_id['id'])
#adding of the results

for i in test_result_data:
    user_session.post(url = 'https://localhost:5000/testrun/'+str(run_id['id'])+'/result', json = i)
###############STEP2##################
#Creating the report
response = user_session.get(url = 'https://localhost:5000/testrun/'+str(run_id['id'])+'/pdf')
with open('report.pdf', 'wb') as f:
    f.write(response.content)

###############STEP3##################



user_session.post(url = 'https://localhost:5000/logout')