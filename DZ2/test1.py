import requests

from requests.packages.urllib3.exceptions import InsecureRequestWarning

user_session = requests.Session() 
user_session.verify = False


requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
user_session.post(url = 'https://localhost:5000/login', json = {'login': 'admin', 'password': 'admin'})

user_session.post(url = 'https://localhost:5000/admin/user', json = {'new_login': 'new_user5', 'new_password': 'user5_password'})
user_session.post(url = 'https://localhost:5000/logout')

user_session.post(url = 'https://localhost:5000/login', json = {'login': 'new_user5', 'password': 'user5_password'})
