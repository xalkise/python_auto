from datetime import datetime

my_file = open('task_1_2.log')
error_buf = []
for line in my_file:
    line = str(line)
    line = line.split()
    try:
        if line[3] == 'Started':          #check for proc duration
            proc = line[7].strip('<')
            my_file2 = open('task_1_2.log')
            for line2 in my_file2:
                line2 = str(line2)
                line2 = line2.split()
                try:
                    if line2[7].strip('<') == proc and line2[3] == 'Finished':
                        start_time = datetime.strptime(str(line[1].strip(']')), '%H:%M:%S.%f')
                        finish_time = datetime.strptime(str(line2[1].strip(']')), '%H:%M:%S.%f')
                        delta = finish_time - start_time
                        print("Request ", proc,": ", delta.total_seconds(), sep = '')
                except Exception:
                    continue
        if line[2] == 'ERROR':                #searching for Errors
            line1 = ' '.join(line)
            st = line1.find('>')
            fi = line1.find('<')
            error_text = line1.find('ERROR') + 5
            error_buf.append(line1[st+1:fi] +":" + line1[error_text: st-1])

    except Exception:
        continue
my_file.close()
print('\n',"Errors: ",sep='')
for error in error_buf:
    print(error)