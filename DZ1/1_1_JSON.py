import requests
import json
response = requests.get('http://www.auriga.com')
full_html = response.content.decode()
import bs4

# Создаем объект парсера
soup = bs4.BeautifulSoup(full_html, 'html.parser')
result=[]
i = 0
for ref in soup.find_all('a', href=True):
    ahreference = dict()
    ahreference["text"] = str(ref.text)
    ahreference["link"] = str(ref['href'])
    result.append(ahreference)

f = open('JSON2.txt', 'w')
f.write(json.dumps(result))
f.close()

