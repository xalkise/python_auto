import subprocess

process = subprocess.Popen(['lsof'], shell=True, stdout=subprocess.PIPE)
pid =1
count = 0
print("PID ","Connections")
while True:
    line_data = process.stdout.readline() # получаем тип bytes
    line = line_data.decode().strip().split() # преобразуем в строку
    if line == '' or process.poll() is not None:
        break
    else:
        if line[1] == pid:
            count +=1
        else:
            print(pid, count)
            pid = line[1]
            count = 1

